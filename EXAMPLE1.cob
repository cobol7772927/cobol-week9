       IDENTIFICATION DIVISION.
       PROGRAM-ID. EXAMPLE1.
       AUTHOR. Kittipon Thaweelarb.
       
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 STAR         PIC *****.
       01 NUM-OF-STAR  PIC 9.
       01 NUM1         PIC VP(5)999.

       PROCEDURE DIVISION.
           PERFORM VARYING NUM-OF-STAR FROM 0 BY 1 UNTIL NUM-OF-STAR > 5
                   COMPUTE STAR = 10 **(4 - NUM-OF-STAR)
                   INSPECT STAR REPLACING ALL "10" BY SPACES 
      *            INSPECT STAR CONVERTING "10" TO SPACES
                   DISPLAY NUM-OF-STAR "=" STAR
           END-PERFORM
           DISPLAY "----------------------------"

           MOVE ".12345678" TO NUM1 
           DISPLAY NUM1
           GOBACK.