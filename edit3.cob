       IDENTIFICATION DIVISION.
       PROGRAM-ID. EDIT3.
       AUTHOR. Kittipon Thaweelarb.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           CURRENCY SIGN IS '$'.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 DOLLARVALUE       PIC 9999V99.

       01 PRNDOLLARVALUE    PIC $$$,$$9.99.
       01 PRNYENVALUE       PIC $$$,$$9.99.
       01 PRNPOUNDVALUE     PIC $$$,$$9.99.

       01 DOLLAR2POUNDRATE  PIC 99V9(6)    VALUE 0.640138.
       01 DOLLARZYENRATE    PIC 99V9(6)    VALUE 98.6600.
       PROCEDURE DIVISION.
           DISPLAY "Enter a dollar value to convert :- "
              WITH NO ADVANCING
           ACCEPT DOLLARVALUE
           
           MOVE DOLLARVALUE TO PRNDOLLARVALUE

           COMPUTE PRNYENVALUE
              ROUNDED = DOLLARVALUE * DOLLARZYENRATE
           COMPUTE PRNPOUNDVALUE
              ROUNDED = DOLLARVALUE * DOLLAR2POUNDRATE

           DISPLAY "Dollar value = "
                   PRNDOLLARVALUE
           DISPLAY "Yen value = "
                   PRNYENVALUE
           DISPLAY "Pound value = "
                   PRNPOUNDVALUE
           GOBACK.