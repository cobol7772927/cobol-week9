       IDENTIFICATION DIVISION.
       PROGRAM-ID. EDIT2.
       AUTHOR. Kittipon Thaweelarb.
       
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
       
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 EDIT1  PIC $$$,$$9.99.
       PROCEDURE DIVISION.
       BEGIN.
           MOVE 123456.95 TO EDIT1
           DISPLAY "Edit1 = " EDIT1
           STOP RUN.